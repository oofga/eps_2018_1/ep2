# Exercício 2 - Orientação a Objetos

## Mapas

Os arquivos que devem ser lidos para a montagem dos tabuleiros estão disponíveis na pasta "maps", e possuem a seguinte estrutura:

#### Altura de Largura (respectivamente) do tabuleiro: 

Inteiros, positivos, separados por espaço em branco, seguidos por uma quebra de linha e uma linha em branco.

Estes valores representam o número de casas em cada linha e coluna do tabuleiro.

#### Matriz: 

Matriz, com a altura e largura definida pelos valores acima, que representa o conteúdo de cada casa do tabuleiro.

Contém inteiros, positivos, onde o número '0'(zero) representa água, e qualquer outro valor maior que '0' indica a presença de uma embarcação, cujo tamanho (quantidade de casas que ocupa) é correspondente ao valor numérico indicado (ex.: '1' representa uma embarcação que ocupa um espaço, '3' representa uma embarcação que ocupa três espaços, etc).

A matriz é seguida de uma linha em branco.

#### Quantidade de embarcações:

Pares de inteiros, positivos, que indicam a quantidade de embarcações de cada tamanho presentes no tabuleiro.

Os pares contém o tamanho da embarcação e a quantidade destes barcos presentes no tabuleiro, separados por um espaço em branco e seguidos de uma quebra de linha.

As embarcações podem ocupar de um a cinco espaços, por isso, o arquivo possui cinco linhas contendo a quantidade de cada tipo de barco.

## Artes

As _sprites_ das embarcações estão disponíveis na pasta "seaWarfareSet". Lembrem-se de referenciar o autor e a licença das imagens.